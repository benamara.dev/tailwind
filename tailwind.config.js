module.exports = {
  purge: {
    layers: ['components', 'utilities'],
    content: [
      './templates/**/*.html.twig',
      './assets/**/*.css',
      './assets/**/*.scss',
      './assets/**/*.js',
    ]
  },
  
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
